// --- change here to benchmark.
// #define BENCH_DO
#define BENCH_FILE "/tmp/cprof.txt"
// ------

#ifdef BENCH_DO
# define BENCH_START(str) bench_start (str);
# define BENCH_END bench_end ();
#else
# define BENCH_START(str)
# define BENCH_END
#endif

#include <stdbool.h>
#include <stdio.h>

#include <sys/time.h>

FILE *bench_f;
const char *bench_str;
double bench_cur;

double get_time_stamp ()
{
    struct timeval tv;
    gettimeofday (&tv, NULL);
    time_t secs = tv.tv_sec;
    suseconds_t tv_usec = tv.tv_usec;
    return (int)secs + tv_usec / 1e6;
}

// --- beware, don't start a new one when one is already running.

void bench_start (const char *str)
{
    bench_f = fopen (BENCH_FILE, "a");
    bench_str = str;
    fprintf (bench_f, "starting %s\n", str);
    bench_cur = get_time_stamp ();
}

void bench_end ()
{
    double d = get_time_stamp ();
    fprintf (bench_f, "%s took %.6f s\n", bench_str, d - bench_cur);
    fclose (bench_f);
    bench_cur = d;
}

unsigned short convert_888_565 (unsigned char r,  unsigned char g, unsigned char b)
{
    unsigned char rs, gs, bs;
    unsigned short rss, gss, bss;
    rs  = r  >> 3;
    gs  = g  >> 2;
    bs  = b  >> 3;
    rss = rs << 11;
    gss = gs << 5;
    bss = bs;

    return rss | gss | bss;
}

// --- on my desktop, takes less than 1 ms for 512 x 512; the FFI call
// has non-trivial overhead, though.
void transfer_888_565 (int num_pixels, unsigned char *from, unsigned short *to)
{
    BENCH_START ("transfer_888_565")
    unsigned char *end = from + num_pixels * 3;
    while (from < end)
        *to++ = convert_888_565 (*(from + 0), *(from + 1), *(from + 2)),
            from += 3;
    BENCH_END
}

void transfer_8888_565 (int num_pixels, unsigned char *from, unsigned short *to)
{
    BENCH_START ("transfer_8888_565")
    unsigned char *end = from + num_pixels * 4;
    while (from < end)
        *to++ = convert_888_565 (*(from + 0), *(from + 1), *(from + 2)),
            from += 4;
    BENCH_END
}

void transfer_888_8880 (int num_pixels, unsigned char *from, unsigned char *to)
{
    BENCH_START ("transfer_888_8880")
    unsigned char *end = from + num_pixels * 3;
    while (from < end) {
        *to++ = *from++;
        *to++ = *from++;
        *to++ = *from++;
        *to++ = 255;
    }
    BENCH_END
}
