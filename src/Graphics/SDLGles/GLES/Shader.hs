module Graphics.SDLGles.GLES.Shader ( uniform
                                    , uniformsMatrixD
                                    , attrib
                                    , initProgram
                                    , useShader
                                    , useShaderMb ) where

import           Prelude hiding ( log, tan )

import           Text.Printf ( printf )
import           Data.Maybe ( isNothing, isJust, fromJust )
import qualified Data.ByteString.Char8  as BS8 ( pack )
import           Control.Monad ( (>=>), (<=<), unless, guard, when, forM_, forM )
import           Data.Monoid ( (<>) )
import           Data.StateVar        as STV ( ($=) )
import qualified Data.StateVar        as STV ( get )

import qualified Graphics.Rendering.OpenGL as GL
                 ( uniform )

import           Graphics.Rendering.OpenGL as GL
                 ( Capability ( Enabled, Disabled )
                 , ShaderType ( VertexShader, FragmentShader )
                 , AttribLocation
                 , UniformLocation
                 , shaderSourceBS
                 , activeTexture
                 , textureBinding
                 , compileShader
                 , shaderInfoLog
                 , createProgram
                 , compileStatus
                 , attachShader
                 , shaderCompiler
                 , linkProgram
                 , linkStatus
                 , programInfoLog
                 , attribLocation
                 , uniformLocation
                 , createShader
                 , currentProgram
                 , vertexAttribArray )

import           Graphics.SDLGles.GLES.Coords ( toMGC )

import           Graphics.SDLGles.Types ( Log (Log, info, warn, err)
                                        , ShaderD ( ShaderDT, ShaderDC )
                                        , appLog
                                        , appMatrix
                                        , shaderD_um
                                        , shaderD_uv
                                        , shaderD_up
                                        )

import           Graphics.SDLGles.GL.Util
                 ( wrapGL )

-- import           Graphics.SDLGles.Util ()

import           Graphics.SDLGles.Util ( stackPop' )
import           Graphics.SDLGles.Util3 ( fst3
                                        , snd3
                                        , thd3
                                        , mapM3 )

uniformsMatrix' app tag um uv up = do
    let log = appLog app
        uniform' = uniform log tag
        appmatrix = appMatrix app
    (model, view, proj) <- mapM3 (toMGC . stackPop') appmatrix
    uniform' um model
    uniform' uv view
    uniform' up proj

uniformsMatrixD app tag shader = uniformsMatrix' app tag um uv up where
    um = shaderD_um shader
    uv = shaderD_uv shader
    up = shaderD_up shader

uniform log tag unif val = wrapGL log tag $ GL.uniform unif $= val
attrib  log tag attr state = wrapGL log str' $ vertexAttribArray attr $= state where
    str' | state == Enabled = "enable vertexAttribArray "  <> tag
         | otherwise        = "disable vertexAttribArray " <> tag

useShader log prog = wrapGL log "use shader" $ currentProgram $= Just prog

useShaderMb log (Just prog) = useShader log prog
useShaderMb log Nothing     = pure ()

initProgram log vShaderSrc fShaderSrc = do
    let err' = err log
        info' = info log
        info'' "" = pure ()
        info'' x = info' x

    canCompile <- wrapGL log "get shaderCompiler" shaderCompiler
    unless canCompile $ err' "Compiling shaders not supported!"
    guard canCompile

    vShader <- wrapGL log "createShader" $ createShader VertexShader
    wrapGL log "shaderSourceBS" $ shaderSourceBS vShader $= vShaderSrc
    wrapGL log "compileShader" $ compileShader vShader
    vcStatus <- STV.get . wrapGL log "compileStatus" $ compileStatus vShader
    info'' =<< STV.get (shaderInfoLog vShader)
    guard vcStatus

    fShader <- wrapGL log "createShader" $ createShader FragmentShader
    wrapGL log "shaderSourceBS" $ shaderSourceBS fShader $= fShaderSrc
    wrapGL log "compileShader" $ compileShader fShader
    fcStatus <- STV.get . wrapGL log "compileStatus" $ compileStatus fShader
    info'' =<< STV.get (shaderInfoLog fShader)
    guard fcStatus

    prog <- wrapGL log "createProgram" createProgram
    wrapGL log "attach vShader" $ attachShader prog vShader
    wrapGL log "attach fShader" $ attachShader prog fShader

    wrapGL log "linkProgram" $ linkProgram prog
    lStatus <- wrapGL log "linkStatus" $ linkStatus prog
    info'' =<< STV.get (programInfoLog prog)
    guard lStatus

    info'' =<< STV.get (shaderInfoLog vShader)
    info'' =<< STV.get (shaderInfoLog fShader)
    info'' =<< STV.get (programInfoLog prog)
    pure prog

