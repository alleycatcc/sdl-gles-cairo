module Graphics.SDLGles.Config ( defaultConfig
                               , doDebug
                               , isEmbedded
                               -- , openGLESCompatibility
                               ) where

import           Foreign.C.Types ( CInt )
import           System.Info as I ( os )

import           Graphics.SDLGles.Types
                 ( Config (Config)
                 , openGLESVer
                 , colorProfile32
                 , output565
                 , windowWidth
                 , windowHeight )

defaultConfig = Config { openGLESVer = openGLESVer_
                       , colorProfile32 = colorProfile32_
                       , output565 = output565_
                       , windowWidth = windowWidth_
                       , windowHeight = windowHeight_
                       }

-- openGLESCompatibility     = False

openGLESVer_ :: (CInt, CInt)
openGLESVer_ = (2, 0)
  -- | openGLESCompatibility = (1, 1)
  -- | otherwise             = (2, 0)

-- 24 is the default, seems to work for all devices.
colorProfile32_           = False

-- 565 is necessary for some mobiles, and seems to work generally.
output565_ = isEmbedded

isEmbedded                = I.os == "linux_android"

windowWidth_ = 480
windowHeight_ = 854

doDebug = False
