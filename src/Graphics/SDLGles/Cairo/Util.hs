module Graphics.SDLGles.Cairo.Util ( cFlipHorizontal
                                   ) where

import qualified Graphics.Rendering.Cairo as C
                 ( Render
                 , transform )

import qualified Graphics.Rendering.Cairo.Matrix as CM
                 ( Matrix (Matrix) )

cFlipHorizontal :: Int -> C.Render ()
cFlipHorizontal = C.transform . cFlipHorizontalMatrix
cFlipHorizontalMatrix width = CM.Matrix (-1) 0 0 1 (fromIntegral width) 0

