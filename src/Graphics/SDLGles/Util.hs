module Graphics.SDLGles.Util ( appReplaceModel
                             , appReplaceView
                             , appReplaceProj
                             , appUpdateModel
                             , appUpdateView
                             , appUpdateProj
                             , replaceModel
                             , replaceView
                             , replaceProj
                             , updateModel
                             , updateView
                             , updateProj
                             , pushModel
                             , pushView
                             , pushProj
                             , popModel
                             , popView
                             , popProj
                             , appMultiplyModel
                             , appMultiplyView
                             , appMultiplyProj
                             , appMultiplyRightModel
                             , appMultiplyRightView
                             , appMultiplyRightProj
                             , appUpdateMatrix
                             , stackReplace'
                             , stackUpdate'
                             , stackPush'
                             , stackPop'
                             , liftA2, liftA3, liftA4, liftA5, liftA6
                             , liftA7, liftA8, liftA9, liftA10, liftA11
                             ) where

import           GHC.Base ( liftA2 )
import           Data.Function ( (&) )
import           Data.Monoid ( (<>) )
import qualified Data.StateVar as STV ( get )
import           Data.Stack ( stackPush, stackPop )

import           Graphics.Rendering.OpenGL as GL
                 ( errors
                 )

import           Graphics.SDLGles.Types ( Log (info, err)
                                        , App
                                        , DMat
                                        , appMatrix
                                        )

import           Graphics.SDLGles.Util2 ( frint
                                        , multMatrices )
import           Graphics.SDLGles.Util3
                 ( up3fst, up3snd, up3thd )

debug | doDebug == True = info
      | otherwise       = const . const . pure $ ()

doDebug = False

replaceModel    = up3fst . stackReplace'
replaceView     = up3snd . stackReplace'
replaceProj     = up3thd . stackReplace'

updateModel     = up3fst . stackUpdate'
updateView      = up3snd . stackUpdate'
updateProj      = up3thd . stackUpdate'

pushModel       = up3fst . stackPush'
pushView        = up3snd . stackPush'
pushProj        = up3thd . stackPush'

popModel        = up3fst . stackPop'
popView         = up3snd . stackPop'
popProj         = up3thd . stackPop'

appReplaceModel = appUpdateMatrix . replaceModel
appReplaceView  = appUpdateMatrix . replaceView
appReplaceProj  = appUpdateMatrix . replaceProj

appUpdateModel  = appUpdateMatrix . updateModel
appUpdateView   = appUpdateMatrix . updateView
appUpdateProj   = appUpdateMatrix . updateProj

appMultiplyModel :: DMat -> App a -> App a
appMultiplyModel matrix' = appUpdateModel f where
    f = \model -> multMatrices [ matrix', model ]
appMultiplyView  matrix' = appUpdateView  f where
    f = \view  -> multMatrices [ matrix', view  ]
appMultiplyProj  matrix' = appUpdateProj  f where
    f = \proj  -> multMatrices [ matrix', proj  ]

appMultiplyRightModel matrix' = appUpdateModel f where
    f = \model -> multMatrices [ model, matrix' ]
appMultiplyRightView  matrix' = appUpdateView  f where
    f = \view  -> multMatrices [ view, matrix' ]
appMultiplyRightProj  matrix' = appUpdateProj  f where
    f = \proj  -> multMatrices [ proj, matrix' ]

appUpdateMatrix f app = app { appMatrix = appMatrix app & f }

stackPush' = flip stackPush
---- these die if the stack is empty.
stackPop'  = snd . maybe (error "stack empty") id . stackPop
stackPop'' = fst . maybe (error "stack empty") id . stackPop
stackReplace' a s = s & stackPop'' & stackPush' a
stackUpdate' f s = s' & stackPush' v' where
    (s', v) = pop'
    v' = f v
    pop' = maybe (error "stack empty") id $ stackPop s

liftA3  f' a b z                 = liftA2  f' a b <*> z
liftA4  f' a b c z               = liftA3  f' a b c <*> z
liftA5  f' a b c d z             = liftA4  f' a b c d <*> z
liftA6  f' a b c d e z           = liftA5  f' a b c d e <*> z
liftA7  f' a b c d e f z         = liftA6  f' a b c d e f <*> z
liftA8  f' a b c d e f g z       = liftA7  f' a b c d e f g <*> z
liftA9  f' a b c d e f g h z     = liftA8  f' a b c d e f g h <*> z
liftA10 f' a b c d e f g h i z   = liftA9  f' a b c d e f g h i <*> z
liftA11 f' a b c d e f g h i j z = liftA10 f' a b c d e f g h i j <*> z
