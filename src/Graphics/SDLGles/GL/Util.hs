module Graphics.SDLGles.GL.Util ( wrapGL
                                ) where

import           Data.Monoid ( (<>) )
import qualified Data.StateVar as STV ( get )

import           Graphics.Rendering.OpenGL as GL
                 ( errors
                 )

import           Graphics.SDLGles.Types ( Log (info, err)
                                        , App
                                        , appMatrix
                                        )
wrapGL log tag action = do
    wrapGLStart log tag
    ret <- action
    wrapGLEnd log tag
    pure ret

wrapGLStart log tag = debug log         $ "GL: doing " <> tag
wrapGLEnd log tag   = checkGLErrors log $ "gl: after " <> tag

checkGLErrors log tag = do
    errs <- STV.get errors
    show' errs where
        show' [] = pure ()
        show' e = err log $ "gl error: " ++ tag ++ ": " ++ show e

debug | doDebug == True = info
      | otherwise       = const . const . pure $ ()

doDebug = False
