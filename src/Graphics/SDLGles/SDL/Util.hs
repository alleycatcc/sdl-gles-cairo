module Graphics.SDLGles.SDL.Util ( checkSDLError
                                 ) where

import           Foreign.C                    ( peekCString )

import qualified SDL.Raw.Error as SRE         ( getError
                                              , clearError )

import           Graphics.SDLGles.Types ( Log (info, err)
                                        , DMat
                                        , GMatD
                                        , appMatrix )

checkSDLError :: Log -> String -> IO ()
checkSDLError log tag = do
    check' =<< peekCString =<< SRE.getError
    SRE.clearError where
        check' "" = pure ()
        check' theError = err log . errString $ theError
        errString = (++) $ tag ++ ": "

