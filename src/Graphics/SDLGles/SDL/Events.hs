{-# LANGUAGE PatternSynonyms #-}

module Graphics.SDLGles.SDL.Events ( processEvents ) where

import           Prelude hiding ( log )

import           Control.Applicative ( empty )
import           Data.Maybe ( fromJust, isJust )
import           Data.Foldable ( find )
import           Foreign ( Int32 )
import           Foreign.C ( CFloat )
import           Text.Printf ( printf )
import           Data.Monoid ( (<>) )

import           SDL.Input.Keyboard.Codes ( pattern KeycodeQ )

import           SDL as S
                 ( Event
                 , EventPayload ( KeyboardEvent, MouseMotionEvent
                                , MouseButtonEvent, MouseWheelEvent
                                , MultiGestureEvent
                                , TouchFingerEvent, TouchFingerMotionEvent )
                 , InputMotion (Pressed, Released)
                 , Point (P)
                 , V2 (V2)
                 , keyboardEventKeyMotion
                 , keysymKeycode
                 , keyboardEventKeysym
                 , mouseMotionEventRelMotion
                 , mouseMotionEventState
                 , mouseWheelEventPos, mouseButtonEventPos
                 , touchFingerEventPos, touchFingerMotionEventRelMotion
                 , mouseButtonEventMotion
                 , touchFingerEventMotion
                 , multiGestureEventDDist
                 , eventPayload
                 , pollEvents )

import           Graphics.SDLGles.Types ( Log (info, warn, err) )
import           Graphics.SDLGles.Util3 ( (<|.>), allPass, inv )

doDebug = False

touchScaleAmount = 200
pinchScaleAmount = 1

processEvents log ( viewportWidth, viewportHeight )= do
    events <- pollEvents

    let debug' = debug log
        qPressed = any eventIsQPress events
        showEvents' [] = pure ()
        showEvents' x = debug' $ "polled events: " <> show x
        dragAmounts :: Maybe (Int, Int)
        dragAmounts = getEventDrag events
        wheelOrPinchAmount = getEventWheelOrPinch events
        click = getEventClick ( frint viewportWidth, frint viewportHeight ) events
        clickRelease = getEventClickRelease ( frint viewportWidth, frint viewportHeight ) events
        clickPress = getEventClickPress ( frint viewportWidth, frint viewportHeight ) events
    showEvents' events

    -- @todo
    pure ( qPressed
         , click
         , clickRelease
         , clickPress
         , if isJust wheelOrPinchAmount then Nothing else dragAmounts
         , wheelOrPinchAmount )

eventMotion (MouseButtonEvent mbe) = mouseButtonEventMotion mbe
eventMotion (TouchFingerEvent tfe) = touchFingerEventMotion tfe

isClickEvent (MouseButtonEvent _) = True
isClickEvent (TouchFingerEvent _) = True
isClickEvent _                    = False

getEventClickRelease = getEventClickType' Released
getEventClickPress   = getEventClickType' Pressed

getEventClickType' type' a b = check' $ getEventClick a b where
    (isYes, isNo) | type' == Pressed  = ((==) Pressed, (==) Released)
                  | type' == Released = ((==) Released, (==) Pressed)

    check' x = do
        (a, b, payload) <- x
        if isYes (eventMotion payload) then pure (a, b, payload) else empty

getEventClick :: (Int32, Int32) -> [Event] -> Maybe (Int32, Int32, EventPayload)
getEventClick ( viewportWidth, viewportHeight ) = get' . find isClickEvent . map eventPayload where
    get' (Just event'@(MouseButtonEvent mbe)) = pure $ getMouse' event' ( mouseButtonEventPos mbe )
    get' (Just event'@(TouchFingerEvent tfe)) = pure $ getTouch' event' ( touchFingerEventPos tfe )
    get' Nothing = empty
    getMouse' event' (P (V2 x y)) = (x, y, event')
    getTouch' event' (P (V2 x y)) = (scale' viewportWidth x, scale' viewportHeight y, event')
    scale' vp' = floor . (* frint vp')

-- looping too many times through events ... these could all be combined.
getEventWheel :: [Event] -> Maybe Int
getEventWheel = get' . find find' . map eventPayload where
    find' (MouseWheelEvent mmevd) = True
    find' _ = False
    get' (Just (MouseWheelEvent mmevd)) = get'' ( mouseWheelEventPos mmevd )
    get' Nothing = empty
    -- { -1, 0, 1 }
    get'' (V2 _ y) = pure . frint $ y

getEventPinch :: [Event] -> Maybe Int
getEventPinch = get' . find find' . map eventPayload where
    find' (MultiGestureEvent mgevd) = True
    find' _ = False
    get' (Just (MultiGestureEvent mgevd)) = get'' ( multiGestureEventDDist mgevd )
    get' Nothing = empty
    get'' = pure . normTouch' -- CFloat
    normTouch' = clamp' . floor . (/ pinchScaleAmount)
    clamp' x
      | x < 0  = min x (inv 1)
      | x >= 0 = max x 1

getEventWheelOrPinch :: [Event] -> Maybe Int
getEventWheelOrPinch = getEventWheel <|.> getEventPinch

getEventDrag :: [Event] -> Maybe (Int, Int)
getEventDrag = get' . find find' . map eventPayload where
    find' (MouseMotionEvent mmevd) = True
    find' (TouchFingerMotionEvent mmevd) = True
    find' _ = False
    get' (Just (MouseMotionEvent mmevd)) = getMouse' ( mouseMotionEventRelMotion mmevd
                                                     , mouseMotionEventState mmevd )
    get' (Just (TouchFingerMotionEvent tfmevd)) = getTouch' ( touchFingerMotionEventRelMotion tfmevd )
    get' Nothing = empty
    -- | no buttons
    getMouse' (_, []) = empty
    -- | any button held down
    -- Int32
    getMouse' (V2 x y, _)   = pure ( fromIntegral x, fromIntegral y )
    -- CFloat, [-1, 1]
    getTouch' (V2 x y)      = pure ( normTouch'' x, normTouch'' y )
    normTouch' = floor . (* touchScaleAmount)
    normTouch'' = normTouch'

eventIsQPress = event' . eventPayload where
    event' (KeyboardEvent kev) = allPass [pressed', q'] kev
    event' _                   = False
    pressed'                   = (== Pressed) . keyboardEventKeyMotion
    q'                         = (== KeycodeQ) . keysymKeycode . keyboardEventKeysym

frint :: (Num b, Integral a) => a -> b
frint = fromIntegral

debug | doDebug == True = info
      | otherwise       = const . const . pure $ ()

