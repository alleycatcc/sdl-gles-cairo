{-# LANGUAGE OverloadedStrings #-}

-- | notes:
--
-- • TextureObjects needs to be created by GL (they are called 'names' or
--   TextureObjects but are actually just an integer internally).
--
-- • single image: the texture is prepared once and bound.
--
-- • multiple images: the texture is prepared on each iteration (or multiple
--   thereof) of the render loop.
--
-- • single image with cairo: draw on top of the image using cairo.
--
-- • no image with cairo: special case of previous one -- just clear the
--   cairo surface before drawing.
--
-- • multiple images with cairo.
--
-- • we manually prepare the backing arrays and copy pixels in using poke.
--   when using cairo, the same backing array (with caveats) is tied to a
--   cairo surface, and then we can just draw on it.
--
-- • the arrays are either CUShort or CUChar arrays. on my android the final
--   internal format has to be '565' format, which is one short per pixel.
--   on desktop, '8888' also works (4 bytes per pixel). so that
--   explains the 555 and 8888 modes.
--
-- • with cairo there's another complication: you can't draw on 565 arrays.
--   so for cairo surfaces on mobile, we use 8888_565: first draw in a 8888
--   array and then copy it to a 565 array. performance is reasonable, even
--   when doing the copy in the render loop.
--
-- • the famous texImage2D call is the one which gets the pixel array into
--   the texture, either once (for single images) or in the render loop (for
--   moving images and/or cairo).
--
--   autogen for textures can be convenient: you can leave off the texCoord
--   calls in between vertices when drawing the polygon.
--   but you lose a lot flexibilty, so we do it manually.
--
--   with gles, a few other caveats:
--   keep the Floats as they are: changing to Double can cause immediate
--   crashes
--   glBegin/glEnd (renderPrimitive) doesn't work: have to push (optionally)
--
--   Quads doesn't exist, but TriangleFan does the same.
--
-- • this seems like an unintuitive way to go about this, especially for
--   haskell, and it's not immediately clear why we use Cairo's
--   createImageSurfaceForData function when we're not necessarily dealing
--   with an image and we don't have any data. image surface is just the
--   generic way to refer to a surface backed by a pixel array. ok but why
--   not just use createImageSurface? it's because you can't (quickly) get
--   the underlying pixels inside the render loop, and GL needs them (in the
--   copyTexImage2D function). the function which gets the pixels is
--   extremely slow and couldn't be used in the render loop.
--
-- • we use the programmable pipeline (shaders, vertex buffers, and no
--   matrix stacks). before all drawing commands, all the matrices need to
--   be passed as uniforms.
-- • we keep model & view separate to allow for better lighting control.
-- • we keep track of all the matrices manually as we do not use any non
--   GLES-2.0 compatible features, and we do not use any glu* functions.
-- • we use stacks to do this, in case it's useful at some point, though up
--   until now the last one on the stack has always been enough.
-- • the old 'preservingMatrix' (= glPushMatrix / glPopMatrix) behavior is
--   basically the default, since our structures are immutable of course:
--   pop the model (/ view / projection), do what you want with it, and pass
--   it to the shaders.
--   for the opposite case, where you want the changes to the matrices to
--   persist, be sure to create a new App structure with the updates.
-- • use frustumF or orthoF to set up the projection, and lookatF to set up
--   the view. lots of manual matrix math to get it working with float and
--   avoiding glu* functions, but it works on android, and gives us a
--   proper right-handed coordinate system.
-- • matrix howto: given fixed-function ('old-style') transforms:
--       T1
--       T2
--       T3
--       <draw>
--   new-style is
--       let model' = multMatrices [ M3, M2, M1, model ]
--       <draw with model>
--   to rotate the flowerpot, then push it down the x-axis:
--   multMatrices [ rotateZ, translateX, model ]
--   either read it forwards in grand, fixed coordinates or backwards in
--   local coordinates.
-- • matrix how to: column-/row-major confusion.
--   • consider a matrix in the natural order, where multiplication goes to
--     the right.
--   • write as a list in natural order.
--   • invert the list using invertMajor'.
--   • load into MD using fromList 4 4
--   • make into MG using toMGC before sending to shader.

module Graphics.SDLGles.App ( init, runLoop ) where

import           Prelude hiding ( log, init )

import           Data.Maybe ( isJust, fromJust )
import           Control.Concurrent ( threadDelay )
import           Data.Monoid ( (<>) )
import           Control.Monad ( when, unless, forM_, forM, join )
import           System.Info   as I           ( os )

import           Data.Function ( (&) )
import           Data.Stack ( stackNew )
import qualified Data.StateVar      as STV ( get )
import           Text.Printf ( printf )

import           Graphics.GL ( glDepthRangef
                             )

import           Graphics.Rendering.OpenGL as GL
                 ( Color4 (Color4)
                 , TextureFunction ( Modulate )
                 , ComparisonFunction ( Less )
                 , Capability ( Enabled, Disabled )
                 , BlendingFactor ( One, OneMinusSrcAlpha, SrcAlpha, SrcAlphaSaturate )
                 , PixelStoreDirection ( Unpack, Pack )
                 , TextureTarget2D ( TextureRectangle, Texture2D )
                 , TextureFilter ( Nearest )
                 , HintTarget (PolygonSmooth)
                 , HintMode (Nicest)
                 , maxCombinedTextureImageUnits
                 , maxTextureImageUnits
                 , clearColor
                 , depthFunc
                 , hint
                 , rowAlignment
                 , blend
                 , blendFunc
                 , viewport
                 )

import qualified Graphics.Rendering.OpenGL as GL
                 -- use our depthRange instead for compatibility with
                 -- Android, because depthRange uses doubles.
                 ( depthRange )

import           SDL as S
                 ( ($=)
                 , Mode (Normal)
                 , WindowGraphicsContext (OpenGLContext)
                 , Profile (Compatibility, ES)
                 , V2 (..)
                 , V4 (..)
                 , initializeAll
                 , createWindow
                 , defaultWindow
                 , defaultOpenGL
                 , glColorPrecision
                 , glDepthPrecision
                 , glStencilPrecision
                 , glMultisampleSamples
                 , glProfile
                 , glCreateContext
                 , glSwapWindow
                 , windowInitialSize
                 , windowGraphicsContext )

import           Graphics.SDLGles.Config
                 ( isEmbedded
                 )

import           Graphics.SDLGles.GLES.Coords
                 ( multMatrices
                 , identityMatrix
                 , lookAtF
                 , translateZ
                 , ver3gld
                 , vec3gld
                 , ver3
                 , vec3
                 , rotateX
                 , rotateY
                 , orthoF
                 , frustumF
                 )

import           Graphics.SDLGles.Types
                 ( App (App)
                 , Log (Log, info, warn, err)
                 , Logger
                 , ProjectionType (ProjectionOrtho, ProjectionFrustum)
                 , mvpConfigProjectionType
                 , mvpConfigTranslateZ
                 , appConfig
                 , appLog
                 , appMatrix
                 , appCommitGL
                 , appUser
                 , openGLESVer
                 , colorProfile32
                 , output565
                 , windowWidth
                 , windowHeight
                 )

import           Graphics.SDLGles.GL.Util
                 ( wrapGL )

import           Graphics.SDLGles.Util
                 ( appReplaceModel
                 , appReplaceView
                 , appReplaceProj
                 , pushModel
                 , pushView
                 , pushProj
                 )

import           Graphics.SDLGles.Util2
                 ( frint )

import           Graphics.SDLGles.Util3
                 ( inv )

import           Graphics.SDLGles.SDL.Util
                 ( checkSDLError )

doDebug = False

-- this is mostly here for testing / legacy; generally the app assumes GLES
-- throughout.
useGLES = True

init (info', warn', error') config appUserData mvpConfig args = do
    let log = Log info' warn' error'

    window <- initSDL log config

    let app = App { appConfig = config
                  , appLog = log
                  , appMatrix = appmatrix
                  , appCommitGL = commit'
                  , appUser = appUserData }

        appmatrix = ( stackNew, stackNew, stackNew )
                    & pushModel identityMatrix
                    & pushView identityMatrix
                    & pushProj identityMatrix

        commit' = wrapGL log "swap window" $ glSwapWindow window

        debug' = debug log

    debug' $ "embedded: " ++ show isEmbedded
    debug' $ "I.os: " ++ I.os

    let app' = app & (appReplaceView $   initView app mvpConfig)
                   & (appReplaceProj =<< initProjection app mvpConfig)

    initGL app' window args

    pure app'

initSDL log config = do
    let debug' = debug log
        checkSDLError' = checkSDLError log
    checkSDLError' "begin"
    debug' "initialising"
    initializeAll
    debug' "initialised"
    checkSDLError' "initializeAll"
    let width' = frint . windowWidth $ config
        height' = frint . windowHeight $ config
    window <- createWindow "AlleyCat" $
        defaultWindow { windowInitialSize = V2 width' height'
                      , windowGraphicsContext = OpenGLContext $ openGLConfig config }

    checkSDLError' "createWindow"
    debug' "created window"
    pure window

initGL app window args = do
    let log = appLog app
        debug' = debug log
        info' = info log

    glCreateContext window

    wrapGL log "clearColor"   $ clearColor               $= Color4 0 0 0 0
    -- intuitive view: block greater depth points with lesser depth ones.
    wrapGL log "depthFunc"    $ depthFunc                $= Just Less

    -- when (not useGLES) . wrapGL log "shade model"        $ shadeModel      $= shadeModel'
    -- when (not useGLES) . wrapGL log "texture function"   $ textureFunction $= textureFunction'

    wrapGL log "enable blend" $ blend                                      $= Enabled
    wrapGL log "blendfunc"    $ blendFunc                                  $= blendFunc'

    forM_ hints' $ \(t, m) -> wrapGL log ("hint " <> show t) $ hint t   $= m

    -- wrapGL log "get active texture" $ do
    --     t <- STV.get activeTexture
    --     info' $ printf "active texture: %s" (show t)

    do p <- STV.get $ rowAlignment Pack
       u <- STV.get $ rowAlignment Unpack
       debug'       $ printf "row alignment: pack = %d, unpack = %d" p u

    -- How OpenGL transfers pixels to/from raw memory. Default is 4.
    -- Only necessary when textures are not multiples of 4; anyway, they
    -- have to be powers of 2, so it's not clear we ever need this.
    -- When manually copying Freetype glyphs to a texture we do need it, but
    -- that never worked anyway.
    -- wrapGL log "row alignment unpack" $ rowAlignment Unpack      $= 1

    unless useGLES $ do
       (dr1, dr2) <- wrapGL log "getting depthRange" $ STV.get GL.depthRange
       info' $ printf "dr1 %s dr2 %s" (show dr1) (show dr2)

    -- ter info.
    -- viewport is automatically initialised to pos 0, 0 and size 480 800
    -- ok to call on android too.
    do  vp <- wrapGL log "get viewport" $ STV.get viewport
        debug' $ printf "viewport: %s" (show vp)

    -- this demonstrates that using doubles in the code is not a problem;
    -- passing them to gl* functions however will cause a crash with GLES.
    debug' "testing doubles"
    let a = 0.34    :: Double
        b = 123.202 :: Double
    debug' $ printf "%.1f * %.1f = %.1f" a b (a * b)

    ---- "Both the vertex shader and fragment processing combined cannot use
    ---- more than MAX COMBINED TEXTURE IMAGE UNITS texture image units. If
    ---- both the vertex shader and the fragment processing stage access the
    ---- same texture image unit, then that counts as using two texture image
    ---- units against the MAX COMBINED TEXTURE IMAGE UNITS limit."
    ---- Note that we don't use vertex texture units -- we only sample the
    ---- texture in the fragment shader.
    ---- Also we can create (it seems) as many texture object names as we
    ---- want -- the limits come in to play when you try to use them
    ---- simultaneously.
    -- on Android Nokia One: 8 / 8
    -- on Linux laptop: 96 / 16
    do  maxCom <- STV.get maxCombinedTextureImageUnits
        maxTex <- STV.get maxTextureImageUnits
        info' $ printf "num combined texture units: %s" (show maxCom)
        info' $ printf "max texture image units: %s" (show maxTex)

    pure ()

runLoop app loop loopVals = loop loopVals >>= runLoop' where
    log = appLog app
    runLoop' Nothing = pure ()
    runLoop' (Just newVals) = do
        appCommitGL app
        threadDelayMs frameInterval
        runLoop app loop newVals

debug | doDebug   = info
      | otherwise = const . const . pure $ ()

openGLConfig config = defaultOpenGL { glColorPrecision = color'
                                    , glDepthPrecision = depth'
                                    , glStencilPrecision = 8
                                    , glMultisampleSamples = samples'
                                    , glProfile = profile' } where

    (glesMaj, glesMin)         = openGLESVer config

    colorProfile32'            = colorProfile32 config
    output565'                 = output565 config

    -- GL_RED_SIZE etc.
    color'    | colorProfile32' = V4 8 8 8 8
              -- | output565      = V4 5 6 5 0
              | output565'     = V4 8 8 8 0
              | otherwise      = V4 8 8 8 0
    profile'  | useGLES        = ES Normal glesMaj glesMin
              | otherwise      = Compatibility Normal 2 1
    -- n > 1: sets num buffers to 1 and num samples to n.
    samples'  | isEmbedded     = 4 -- anti-alias 4x FSAA
              | otherwise      = 4
    depth'    | isEmbedded     = 24 -- 16 was default
              | otherwise      = 24

-- must be Modulate if trying to use lighting on a texture.
textureFunction'              = Modulate
blendFunc'                    = (SrcAlpha, OneMinusSrcAlpha)
hints' | useGLES              = []
       | otherwise            = [(PolygonSmooth, Nicest)]



initProjection app mvpConfig = do
    let log = appLog app
        info' = info log
        debug' = debug log
        project ProjectionOrtho   = orthoF   r t n f
        project ProjectionFrustum = frustumF r t n f

    -- can use this to check results against gluFrustum.
    -- wrapGL log "matrixMode Projection" $ matrixMode $= Projection
    -- wrapGL log "loadIdentity" $ loadIdentity
    -- wrapGL log "gluFrustum" $ frustum l r b t n f

    pure . project $ mvpConfigProjectionType mvpConfig

    where
        r =     1
        t =     1
        n =     1.5
        f =     10

-- initialise a right-handed view, with positive z out of the screen, and
-- push the model down the negative z axis a bit.

initView app mvpConfig = multMatrices m where
    log = appLog app
    m = [ lookAtF log v1 v2 v3
        , translateZ tz ]

    v1d = ver3gld 0 0 lzd
    v2d = ver3gld 0 0 0
    v3d = vec3gld 0 1 0

    v1 = ver3 0 0 lz
    v2 = ver3 0 0 0
    v3 = vec3 0 1 0

    lz = 0.4
    tz = mvpConfigTranslateZ mvpConfig

    lzd = 0.4

-- | depthRange doesn't seem to be necessary in conjunction with
--   glFrustrum.
-- | if you do want to set it, be sure to use our version, which uses
--   floats, not GL.depthrange, which uses doubles.

depthRange :: (Float, Float) -> IO ()
depthRange = uncurry glDepthRangef

-- • mapping from eye coordinates to NDC.
-- • near plane of 1.5 gives a decent FOV (no fish-eye), but we have to push
--   the model away a bit down the z-axis during initView.
-- • eye coordinates are right-handed and go from [-1, -1, ?] to [1, 1, ?].
-- • NDC are also right-handed and go from [-1, -1, -1] to [1, 1, 1]
-- • far plane doesn't change the FOV, only how much depth there is:
--   keep it small because otherwise there is z-fighting and the depth buffer
--   gets messed up.
-- • the perspective / gluPerspective call is more intuitive, but we use
--   frustum.
-- • on android we don't have the frustum call, so we roll it by hand using
--   matrix math; plus it works nicely with the programmable pipeline.


-- xxx
frameInterval = 50
threadDelayMs = threadDelay . (* 1000)
