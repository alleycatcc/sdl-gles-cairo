{-# LANGUAGE GADTs #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE PackageImports #-}

module Graphics.SDLGles.Types ( App (App)
                              , Config (Config)
                              , Log (Log)
                              , Logger
                              , DMat
                              , GMat
                              , GMatD
                              , DrawInfo (DrawVertex, DrawColor, DrawTexCoord, DrawNormal)
                              , Shader (Shader)
                              , ShaderD (ShaderDC, ShaderDT)
                              , Tex (NoTexture)
                              , GraphicsTextureMapping (GraphicsTextureMapping)
                              , GraphicsData (GraphicsSingle, GraphicsSingleCairo, GraphicsMoving, GraphicsMovingFinite)
                              , ProjectionType (ProjectionFrustum, ProjectionOrtho)
                              , MVPConfig (MVPConfig)
                              , Attrib (Attrib)
                              , Uniform (Uniform)
                              , MatrixVarsClass
                                ( matrixVarsModel, matrixVarsView, matrixVarsProjection )
                              , ShaderVarsClass
                                ( shaderVarsCAp, shaderVarsCAc, shaderVarsCAn
                                , shaderVarsTAp, shaderVarsTAtc, shaderVarsTAn
                                , shaderVarsTUt )
                              , info
                              , warn
                              , err
                              , shader'Program
                              , shader'ShaderVars
                              , shader'Matrix
                              , shaderD_um
                              , shaderD_uv
                              , shaderD_up
                              , shaderDCProgram
                              , shaderDCUniformModel
                              , shaderDCUniformView
                              , shaderDCUniformProjection
                              , shaderDCAttributePosition
                              , shaderDCAttributeNormal
                              , shaderDCAttributeColors
                              , shaderDTProgram
                              , shaderDTUniformModel
                              , shaderDTUniformView
                              , shaderDTUniformProjection
                              , shaderDTUniformTexture
                              , shaderDTAttributePosition
                              , shaderDTAttributeNormal
                              , shaderDTAttributeTexCoord
                              , toShaderDC
                              , toShaderDT
                              , attribName
                              , attribAttribLocation
                              , uniformName
                              , uniformUniformLocation
                              , tex565GLPixelData
                              , tex565RawPixelData
                              , tex8888GLPixelData
                              , tex8888RawPixelData
                              , tex8888_565GLPixelData565
                              , tex8888_565RawPixelData565
                              , tex8888_565GLPixelData8888
                              , tex8888_565RawPixelData8888
                              , tex8888_565CairoSurf
                              , graphicsTextureMappingGraphicsData
                              , graphicsTextureMappingTexture
                              , graphicsTextureMappingTextureObject
                              , graphicsSingleImage
                              , graphicsSingleDirty
                              , graphicsSingleCairoImage
                              , graphicsSingleCairoFrames
                              , graphicsMovingImages
                              , graphicsMovingFiniteImages
                              , graphicsMovingFiniteNumImages
                              , graphicsMovingFiniteTicksPerImageFrame
                              , graphicsMovingFiniteTicksElapsed
                              , getCSurf
                              , newTex565
                              , newTex8888WithCairo
                              , newTex8888NoCairo
                              , newTex8888_565WithCairo
                              , isTex8888
                              , isTex565
                              , isTex8888_565
                              , openGLESVer
                              , colorProfile32
                              , output565
                              , appConfig
                              , appLog
                              , appCommitGL
                              , appMatrix
                              , appUser
                              , windowWidth
                              , windowHeight
                              , drawInfoAttribLocation
                              , drawInfoVertexCoords
                              , drawInfoColorCoords
                              , drawInfoTexCoords
                              , drawInfoNormal
                              , texWidth
                              , texHeight
                              , mvpConfigProjectionType
                              , mvpConfigTranslateZ
                              , mvpConfigCubeScale
                              ) where

import           Data.Function ( (&) )
import           Foreign.C.Types ( CInt, CUShort, CUChar )
import           Foreign ( Ptr, mallocArray )

import           Data.Stack ( Stack )
import "matrix"  Data.Matrix        as DMX ( Matrix )

import           Codec.Picture      as JP ( DynamicImage )
import qualified Graphics.Rendering.Cairo as C
                 ( Format (FormatARGB32)
                 , Surface
                 , Render
                 , createImageSurfaceForData
                 )

import           Graphics.Rendering.OpenGL as GL
                 ( GLmatrix
                 , PixelData ( PixelData )
                 , Program
                 , TextureObject
                 , PixelFormat ( RGB, RGBA )
                 , DataType ( UnsignedByte, UnsignedShort565 )
                 , UniformLocation
                 , PixelData
                 , GLfloat
                 , GLdouble
                 , AttribLocation
                 , Vertex3
                 , Vertex4
                 )


data App a = App { appConfig :: Config
                 , appLog :: Log
                 , appCommitGL :: IO ()
                   -- | we store DMX matrices, which are easier to work
                   --   with, multiply etc.
                   -- | note that they need to be converted to GLmatrix
                   --   using toMGC before sending to shaders.
                 , appMatrix :: ( Stack (Matrix Float)
                                , Stack (Matrix Float)
                                , Stack (Matrix Float) )
                 , appUser :: a }

data Config = Config { openGLESVer :: (CInt, CInt)
                     , colorProfile32 :: Bool
                     , output565 :: Bool
                     , windowWidth :: Int
                     , windowHeight :: Int }

type Logger = String -> IO ()

data Log = Log { info :: Logger
               , warn :: Logger
               , err :: Logger }

type DMat   = DMX.Matrix Float
type GMat   = GLmatrix GLfloat
type GMatD  = GLmatrix GLdouble

data DrawInfo       = DrawVertex   AttribLocation [Vertex3 Float]
                    | DrawColor    AttribLocation [Vertex4 Float]
                    | DrawTexCoord AttribLocation [Vertex4 Float]
                    | DrawNormal   AttribLocation [Vertex4 Float]

drawInfoAttribLocation (DrawVertex   al _) = al
drawInfoAttribLocation (DrawColor    al _) = al
drawInfoAttribLocation (DrawTexCoord al _) = al
drawInfoAttribLocation (DrawNormal   al _) = al

drawInfoVertexCoords   (DrawVertex   _ c) = c
drawInfoColorCoords    (DrawColor    _ c) = c
drawInfoTexCoords      (DrawTexCoord _ c) = c
drawInfoNormal         (DrawNormal   _ c) = c

-- | shader structures for sending to primitive drawing routines in `Draw`,
--   intentionally decoupled from our main `Shader` type.
-- • set `program` to `Nothing` if you don't want the lower-level routine to
--   'use' it.
-- • the `DT` variant includes a uniform for texture sampler and an
--   attribute for texture coordinates; the `DT` variant has no sampler and
--   an attribute for colors.

data ShaderD = ShaderDC { shaderDCProgram           :: Maybe Program
                        , shaderDCUniformModel      :: UniformLocation
                        , shaderDCUniformView       :: UniformLocation
                        , shaderDCUniformProjection :: UniformLocation
                        , shaderDCAttributePosition :: AttribLocation
                        , shaderDCAttributeColors   :: AttribLocation
                        , shaderDCAttributeNormal   :: AttribLocation }
             | ShaderDT { shaderDTProgram           :: Maybe Program
                        , shaderDTUniformModel      :: UniformLocation
                        , shaderDTUniformView       :: UniformLocation
                        , shaderDTUniformProjection :: UniformLocation
                        , shaderDTUniformTexture    :: UniformLocation
                        , shaderDTAttributePosition :: AttribLocation
                        , shaderDTAttributeTexCoord :: AttribLocation
                        , shaderDTAttributeNormal   :: AttribLocation }

shaderD_um (ShaderDC _ um _  _  _ _ _) = um
shaderD_um (ShaderDT _ um _  _  _ _ _ _) = um
shaderD_uv (ShaderDC _ _  uv _  _ _ _) = uv
shaderD_uv (ShaderDT _ _  uv _  _ _ _ _) = uv
shaderD_up (ShaderDC _ _  _  up _ _ _) = up
shaderD_up (ShaderDT _ _  _  up _ _ _ _) = up

         -- draw directly to 565 using JP (not for cairo).
data Tex = Tex565 { tex565GLPixelData :: PixelData CUShort
                  , tex565RawPixelData :: Ptr CUShort
                  , tex565Width :: Int
                  , tex565Height :: Int }
         -- draw to 8888 using JP and/or cairo (not for mobile).
         | Tex8888 { tex8888GLPixelData :: PixelData CUChar
                   , tex8888RawPixelData :: Ptr CUChar
                   , tex8888CairoSurf :: Maybe C.Surface
                   , tex8888Width :: Int
                   , tex8888Height :: Int }

         -- draw to 8888 using JP and/or cairo (not for mobile).
         | Tex8888_Luminance { tex8888_LuminanceGLPixelData :: PixelData CUChar
         , tex8888_LuminanceRawPixelData :: Ptr CUChar
         , tex8888_LuminanceCairoSurf :: Maybe C.Surface }

         -- draw to 8888 using JP and/or cairo and copy to 565
         | Tex8888_565 { tex8888_565GLPixelData565 :: PixelData CUShort
                       , tex8888_565RawPixelData565 :: Ptr CUShort
                       , tex8888_565GLPixelData8888 :: PixelData CUChar
                       , tex8888_565RawPixelData8888 :: Ptr CUChar
                       , tex8888_565CairoSurf :: Maybe C.Surface
                       , tex8888_565Width :: Int
                       , tex8888_565Height :: Int }
         | NoTexture

texWidth tex@(Tex565 _ _ _ _) = tex565Width tex
texWidth tex@(Tex8888 _ _ _ _ _) = tex8888Width tex
texWidth tex@(Tex8888_565 _ _ _ _ _ _ _) = tex8888_565Width tex

texHeight tex@(Tex565 _ _ _ _) = tex565Height tex
texHeight tex@(Tex8888 _ _ _ _ _) = tex8888Height tex
texHeight tex@(Tex8888_565 _ _ _ _ _ _ _) = tex8888_565Height tex

data GraphicsTextureMapping = GraphicsTextureMapping
    { graphicsTextureMappingGraphicsData :: GraphicsData
    , graphicsTextureMappingTexture :: Tex
    , graphicsTextureMappingTextureObject :: TextureObject }

instance Show GraphicsTextureMapping where
    show (GraphicsTextureMapping graphicsData tex obj) = "GraphicsTextureMapping: [data] [tex]" ++ " " ++ show obj

data GraphicsData
      -- single image, no cairo.
    = GraphicsSingle       { graphicsSingleImage       :: JP.DynamicImage
                           , graphicsSingleDirty       :: Bool }
      -- single image or no image, and cairo.
    | GraphicsSingleCairo  { graphicsSingleCairoImage  :: Maybe JP.DynamicImage
                           , graphicsSingleCairoFrames :: [C.Render ()] }
      -- moving images, no cairo, infinite frames.
    | GraphicsMoving       { graphicsMovingImages      :: [JP.DynamicImage]
                           , graphicsMovingTicksPerImageFrame :: Int
                           , graphicsMovingTicksElapsed :: Int }
      -- moving images, no cairo, finite frames.
    | GraphicsMovingFinite { graphicsMovingFiniteImages      :: [JP.DynamicImage]
                           , graphicsMovingFiniteNumImages :: Int
                           , graphicsMovingFiniteTicksPerImageFrame :: Int
                           , graphicsMovingFiniteTicksElapsed :: Int }
--       -- moving images, and cairo.
--     | GraphicsMovingCairo { graphicsMovingCairoImage  :: [JP.DynamicImage]
--                           , graphicsMovingCairoFrames :: [C.Render ()]
--                           , graphicsMovingCairoTicksPerImageFrame :: Int
--                           , graphicsMovingCairoTicksPerCairoFrame :: Int }

getCSurf (Tex565 _ _ _ _) = Nothing
getCSurf (Tex8888 _ _ Nothing _ _) = Nothing
getCSurf (Tex8888 _ _ (Just x) _ _) = Just x
-- getCSurf (Tex8888_Luminance _ _ Nothing) = Nothing
-- getCSurf (Tex8888_Luminance _ _ (Just x)) = Just x
getCSurf (Tex8888_565 _ _ _ _ Nothing _ _) = Nothing
getCSurf (Tex8888_565 _ _ _ _ (Just x) _ _) = Just x
getCSurf NoTexture = Nothing

newTex565 w h = do
    rawPixelData <- mallocArray $ w * h :: IO (Ptr CUShort)
    let glPixelData = PixelData GL.RGB GL.UnsignedShort565 rawPixelData
    pure $ Tex565 glPixelData rawPixelData w h

newTex8888WithCairo w h = do
    rawPixelData <- mallocArray $ w * h * 4 :: IO (Ptr CUChar)
    let glPixelData = PixelData GL.RGBA GL.UnsignedByte rawPixelData
    cSurf' <- C.createImageSurfaceForData rawPixelData C.FormatARGB32 w h $ w * 4
    pure $ Tex8888 glPixelData rawPixelData (Just cSurf') w h

newTex8888NoCairo w h = do
    rawPixelData <- mallocArray $ w * h * 4 :: IO (Ptr CUChar)
    let glPixelData = PixelData GL.RGBA GL.UnsignedByte rawPixelData
    pure $ Tex8888 glPixelData rawPixelData Nothing w h

-- newTex8888_LuminanceWithCairo w h = undefined
-- newTex8888_LuminanceNoCairo w h = do
--     rawPixelData <- mallocArray $ w * h  :: IO (Ptr CUChar)
--     let glPixelData = PixelData GL.Luminance GL.UnsignedByte rawPixelData
--     pure $ Tex8888_Luminance glPixelData rawPixelData Nothing
--
-- newTex8888_565NoCairo is technically possible but not implemented because
-- it doesn't seem useful currently.

newTex8888_565WithCairo w h = do
    t565 <- newTex565 w h
    t8888 <- newTex8888WithCairo w h
    let a = tex565GLPixelData t565
        b = tex565RawPixelData t565
        c = tex8888GLPixelData t8888
        d = tex8888RawPixelData t8888
        e = tex8888CairoSurf t8888
    pure $ Tex8888_565 a b c d e w h

isTex8888 (Tex8888 _ _ _ _ _) = True
isTex8888 _ = False

isTex565 (Tex565 _ _ _ _) = True
isTex565 _ = False

isTex8888_565 (Tex8888_565 _ _ _ _ _ _ _) = True
isTex8888_565 _ = False

data MVPConfig = MVPConfig { mvpConfigProjectionType :: ProjectionType
                           , mvpConfigTranslateZ :: Float
                           , mvpConfigCubeScale :: Float }

data ProjectionType = ProjectionFrustum
                    | ProjectionOrtho

class MatrixVarsClass a where
    matrixVarsModel      :: a -> Uniform
    matrixVarsView       :: a -> Uniform
    matrixVarsProjection :: a -> Uniform

class ShaderVarsClass a where
    shaderVarsCAp        :: a -> Attrib
    shaderVarsCAc        :: a -> Attrib
    shaderVarsCAn        :: a -> Attrib
    shaderVarsTAp        :: a -> Attrib
    shaderVarsTAtc       :: a -> Attrib
    shaderVarsTAn        :: a -> Attrib
    shaderVarsTUt        :: a -> Uniform

-- GADTs allows us to add this constraint here, also without needing the
-- extended 'data ... where' syntax.

data Shader a b = (MatrixVarsClass a, ShaderVarsClass b) =>
    Shader { shader'Program    :: Program
           , shader'Matrix     :: a
           , shader'ShaderVars :: b }

-- it's not possible to use deriving on the data declaration due to the
-- constraints, hence the standalone deriving statement.
deriving instance (Eq a, Eq b) => Eq (Shader a b)
deriving instance (Show a, Show b) => Show (Shader a b)

data Attrib = Attrib { attribName :: String
                     , attribAttribLocation :: AttribLocation }
                     deriving (Show, Eq)

data Uniform = Uniform { uniformName :: String
                       , uniformUniformLocation :: UniformLocation }
                        deriving (Show, Eq)

toShaderDC progMb matrix vars =
    ShaderDC progMb model view projection ap ac an where
        model      = uniformUniformLocation . matrixVarsModel      $ matrix
        view       = uniformUniformLocation . matrixVarsView       $ matrix
        projection = uniformUniformLocation . matrixVarsProjection $ matrix
        ap         = attribAttribLocation   . shaderVarsCAp        $ vars
        ac         = attribAttribLocation   . shaderVarsCAc        $ vars
        an         = attribAttribLocation   . shaderVarsCAn        $ vars

toShaderDT progMb matrix vars =
    ShaderDT progMb model view projection utt ap at an where
        model      = uniformUniformLocation . matrixVarsModel      $ matrix
        view       = uniformUniformLocation . matrixVarsView       $ matrix
        projection = uniformUniformLocation . matrixVarsProjection $ matrix
        utt        = uniformUniformLocation . shaderVarsTUt        $ vars
        ap         = attribAttribLocation   . shaderVarsTAp        $ vars
        at         = attribAttribLocation   . shaderVarsTAtc       $ vars
        an         = attribAttribLocation   . shaderVarsTAn        $ vars

