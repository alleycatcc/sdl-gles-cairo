module Graphics.SDLGles.Texture ( updateTextures
                                , updateTexture
                                , updateTextureCairo
                                , textureWithCairo
                                , textureNoCairo
                                , activateTexture
                                , createTextures
                                , createTextures2DSimple
                                ) where

import           Data.Monoid ( (<>) )
import           Control.Monad ( when, forM_ )
import           Data.Maybe ( isJust, fromJust )
import           Data.StateVar ( ($=) )

import           Graphics.Rendering.OpenGL as GL
                 ( TextureObject
                 , TextureUnit (TextureUnit)
                 , TextureTarget2D ( TextureRectangle, Texture2D )
                 , TextureSize2D ( TextureSize2D )
                 , TextureFilter ( Nearest )
                 , Proxy ( NoProxy )
                 , PixelInternalFormat
                   ( RGBA', RGBA8, RGB8, RGB' {-- GL_RGB--} )
                 , activeTexture
                 , texImage2D
                 , textureBinding
                 , textureFilter
                 , genObjectNames
                 )

import           Graphics.Rendering.Cairo as C
                 ( Render
                 , Operator (OperatorClear, OperatorSource)
                 , renderWith
                 , setSourceRGBA
                 , rectangle
                 , fill
                 , setOperator
                 )

import           Graphics.SDLGles.Pixels
                 ( copyJPImageToPixelArray8888_RGBA
                 , copyJPImageToPixelArray565
                 , copyRaw8888To565 )

import           Graphics.SDLGles.GLES.Shader
                 ( uniform )

import           Graphics.SDLGles.Types
                 ( App
                 , GraphicsTextureMapping (GraphicsTextureMapping)
                 , GraphicsData (GraphicsSingle, GraphicsMoving, GraphicsSingleCairo, GraphicsMovingFinite)
                 , Tex
                 , appLog
                 , output565
                 , texWidth
                 , texHeight
                 , getCSurf
                 , graphicsMovingFiniteTicksElapsed
                 , graphicsMovingFiniteTicksPerImageFrame
                 , graphicsMovingFiniteImages
                 , graphicsMovingFiniteNumImages
                 , isTex8888
                 , isTex565
                 , isTex8888_565
                 , tex565GLPixelData
                 , tex8888GLPixelData
                 , tex565GLPixelData
                 , tex565RawPixelData
                 , tex8888GLPixelData
                 , tex8888RawPixelData
                 , tex8888_565GLPixelData565
                 , tex8888_565RawPixelData565
                 , tex8888_565GLPixelData8888
                 , tex8888_565RawPixelData8888
                 , newTex8888WithCairo
                 , newTex8888NoCairo
                 , newTex8888_565WithCairo
                 , newTex565 )

import           Graphics.SDLGles.GL.Util
                 ( wrapGL )

import           Graphics.SDLGles.Util2
                 ( frint )

-- | bind the texture, set it to 'active', and pass its sampler to the
-- shader via a uniform.

activateTexture log texName utt = do
    let tag1 = show texUnit'
        tag2 = show texName
        texUnit' = 0

    -- | both calls are necessary.
    -- • we always use TextureUnit 0, which is a different space than texture objects and texture
    --   targets.
    -- • we have at least 8 texture units (see max texture image units)
    -- we set unit 0 to be active, bind a texname to it, and render it.
    -- then the fragment shader can access it.
    -- the fragment shader can access <max> texture units in one draw call.

    wrapGL log ("set activeTexture " <> tag1) $ activeTexture $= TextureUnit texUnit'
    wrapGL log ("textureBinding " <> tag2) $ textureBinding Texture2D $= Just texName

    uniform log ("utt " <> tag1) utt $ TextureUnit texUnit'


updateTextures :: App a -> [GraphicsTextureMapping] -> IO [GraphicsTextureMapping]
updateTextures app = mapM map' where
    map' (GraphicsTextureMapping graphicsData tex texobj) = do
        graphicsData' <- updateTexture app graphicsData tex texobj
        pure $ GraphicsTextureMapping graphicsData' tex texobj

updateTexture :: App a -> GraphicsData -> Tex -> GL.TextureObject -> IO GraphicsData
updateTexture app graph@(GraphicsSingle img False) tex texobj = pure graph
updateTexture app       (GraphicsSingle img True) tex texobj = do
    transferImageToTexture app img tex texobj
    texImage' app tex texobj
    pure $ GraphicsSingle img False

updateTexture app       (GraphicsMoving imgs ticks ticksElapsed) tex texobj
  | ticksElapsed == 0 = do
        head' <- case imgs of
                   [] -> fail "updateTexture: empty imgs"
                   x : _ -> pure x
        transferImageToTexture app head' tex texobj
        texImage' app tex texobj
        let imgs' = tail'' imgs
        pure $ GraphicsMoving imgs' ticks te'
  | otherwise = do
        let imgs' = imgs
        pure $ GraphicsMoving imgs' ticks te' where
    te' = (ticksElapsed + 1) `mod` ticks

-- numImages can be smaller than length of imgs; also `length` is expensive.
updateTexture app graph@(GraphicsMovingFinite imgs numImages ticks ticksElapsed) tex texobj
  | numImages == 0 = pure graph
  | ticksElapsed == 0 = do
        (head', tail') <- case imgs of
                   [] -> fail "updateTexture: empty imgs"
                   x : xs -> pure (x, xs)
        transferImageToTexture app head' tex texobj
        texImage' app tex texobj
        pure $ GraphicsMovingFinite tail' (numImages - 1) ticks te'
  | otherwise = do
        pure $ graph { graphicsMovingFiniteTicksElapsed = te' } where
    te' = (ticksElapsed + 1) `mod` ticks

updateTexture app       (GraphicsSingleCairo Nothing cFrames) tex texobj = do
    head' <- case cFrames of
               [] -> fail "updateTexture: cFrames is empty"
               x : _ -> pure x
    let width = texWidth tex
        height = texHeight tex
    updateTextureCairo True tex head' width height
    texImage' app tex texobj
    pure $ GraphicsSingleCairo Nothing (tail'' cFrames)

updateTexture app       (GraphicsSingleCairo (Just img) cFrames) tex texobj = do
    head' <- case cFrames of
               [] -> fail "updateTexture: cFrames is empty"
               x : _ -> pure x
    transferImageToTexture app img tex texobj
    let width = texWidth tex
        height = texHeight tex
    updateTextureCairo False tex head' width height
    texImage' app tex texobj
    pure $ GraphicsSingleCairo (Just img) (tail'' cFrames)

updateTextureCairo clear tex cFrame textureWidth textureHeight = do
    let cSurf' = getCSurf tex
    when (isJust cSurf') .
        C.renderWith (fromJust cSurf') $ renderToCairoSurface clear textureWidth textureHeight cFrame

-- | actually transfer the pixel array into the texture.
--   all textures need to do this at least once.

texImage' app tex texobj
  | isTex8888 tex     = let gl = tex8888GLPixelData tex
                            w = texWidth tex
                            h = texHeight tex
                        in  texImage8888' w h texobj gl
  | isTex565 tex      = let gl = tex565GLPixelData tex
                            w = texWidth tex
                            h = texHeight tex
                        in  texImage565' w h texobj gl
                            -- | isTex8888_Luminance tex = let gl = tex8888_LuminanceGLPixelData tex  in texImage8888_Luminance' texobj gl
  | isTex8888_565 tex =                                    texImage8888_565' app texobj tex
  | otherwise         = error "texImage': unknown type"

texImage'' internalFormat width height texobj gl = do
    let texWidth' = frint width
        texHeight' = frint height
    let level' = 0
        texImage2D' = texImage2D Texture2D NoProxy level' internalFormat (TextureSize2D texWidth' texHeight') 0
    textureBinding Texture2D $= Just texobj
    texImage2D' gl

texImage8888'     = texImage'' GL.RGBA8 -- GL_RGBA
texImage565'      = texImage'' GL.RGB' -- GL_RGB

texImage8888_565' app texobj tex = do
    let log = appLog app
        ary8888 = tex8888_565RawPixelData8888 tex
        ary565 = tex8888_565RawPixelData565 tex
        gl565 = tex8888_565GLPixelData565 tex
        w = texWidth tex
        h = texHeight tex
    copyRaw8888To565 log w h ary8888 ary565
    texImage565' w h texobj gl565

-- | copy an image into the texture.
-- | only used for faces which need a background image.

transferImageToTexture app img tex texobj
  | isTex8888 tex = do
      let raw = tex8888RawPixelData tex
          w = texWidth tex
          h = texHeight tex
      copyJPImageToPixelArray8888_RGBA w h img raw
  | isTex565 tex = do
      let log = appLog app
          raw = tex565RawPixelData tex
          w = texWidth tex
          h = texHeight tex
      copyJPImageToPixelArray565 log w h img raw
  | isTex8888_565 tex = do
      -- transfer the image to the 8888 array, so it can still be drawn on
      -- by cairo, and then all transferred togther to 565.
      let raw = tex8888_565RawPixelData8888 tex
          w = texWidth tex
          h = texHeight tex
      copyJPImageToPixelArray8888_RGBA w h img raw
  | otherwise = error "transferImageToTexture: unknown type"

tail'' [x] = [x]
tail'' (_ : xs) = xs

renderToCairoSurface :: Bool -> Int -> Int -> C.Render () -> C.Render ()
renderToCairoSurface clear width height frame = do
    -- operatorSave <- C.getOperator
    let clear' = do
            C.setSourceRGBA 1 1 1 1
            C.rectangle 0 0 (fromIntegral width) (fromIntegral height)
            C.fill
    let clear'' = do
            C.setOperator C.OperatorClear
            C.rectangle 0 0 (fromIntegral width) (fromIntegral height)
            C.fill
            C.setOperator C.OperatorSource

    when clear clear''
    frame

textureWithCairo config | output565 config = newTex8888_565WithCairo
                        | otherwise = newTex8888WithCairo

textureNoCairo config   | output565 config = newTex565
                        | otherwise = newTex8888NoCairo

createTextures target min mag app num = do
    let log = appLog app
    texNames <- wrapGL log "generating texture objects" $ genObjectNames num

    forM_ texNames $ \texName -> do
        textureBinding target $= Just texName
        wrapGL log "texture filter" $ textureFilter target  $= (min, mag)
    pure texNames

-- | it is important to set a texture filter.
--   `Nearest` = quick & dirty.
createTextures2DSimple = createTextures Texture2D (Nearest, Nothing) Nearest
