-- | internal utils but you are free to use them.

{-# LANGUAGE PackageImports #-}

module Graphics.SDLGles.Util3 ( (<|.>)
                              , allPass
                              , isMultiple
                              , isInteger
                              , up3fst
                              , up3snd
                              , up3thd
                              , vcross
                              , vdot
                              , vmag
                              , vdiv
                              , fst3
                              , snd3
                              , thd3
                              , map3
                              , mapM3
                              , float
                              , toDeg
                              , inv
                              , deg2rad
                              , v3x
                              , v3y
                              , v3z
                              , concatTuples2
                              , concatTuples3
                              , concatTuples4
                              , concatVectors3
                              , concatVectors4
                              , concatVertex1
                              , concatVertex2
                              , concatVertex3
                              , concatVertex4
                              ) where

import           Control.Applicative ( (<|>) )
import           Data.Function ( (&) )
import           Data.Foldable ( find )

import           Graphics.SDLGles.Util2 ( frint )

import           Graphics.Rendering.OpenGL as GL
                 ( Vector3 ( Vector3 )
                 , Vector4 ( Vector4 )
                 , Vertex1 ( Vertex1 )
                 , Vertex2 ( Vertex2 )
                 , Vertex3 ( Vertex3 )
                 , Vertex4 ( Vertex4 ) )

isMultiple :: (Integral a, Integral b) => a -> b -> Bool
isMultiple m n = isInteger $ frint n / frint m

isInteger :: RealFrac a => a -> Bool
isInteger a = (== a) . realToFrac  . floor $ a

vcross (Vector3 a b c) (Vector3 x y z) = Vector3 x' y' z' where
    x' = b * z - c * y
    y' = c * x - a * z
    z' = a * y - b * x

vmag (Vector3 a b c) = sqrt $ a ** 2 + b ** 2 + c ** 2

vdot (Vector3 a b c) (Vector3 x y z) = x' + y' + z' where
    x' = a * x
    y' = b * y
    z' = c * z

vdiv n (Vector3 a b c) = Vector3 a' b' c' where
    a' = a / n
    b' = b / n
    c' = c / n

up3fst f (a, b, c) = (f a, b, c)
up3snd f (a, b, c) = (a, f b, c)
up3thd f (a, b, c) = (a, b, f c)

fst3 (a, b, c) = a
snd3 (a, b, c) = b
thd3 (a, b, c) = c

mapM3 f (a, b, c) = (,,) <$> f a <*> f b <*> f c
map3 f (a, b, c) = (f a, f b, f c)

float x = x :: Float

toDeg :: Float -> Float
toDeg = (* 180) . (/ pi)

inv x = (-1) * x

deg2rad :: Float -> Float
deg2rad = (/ 180.0) . (* pi)

v3x (Vector3 x _ _ ) = x
v3y (Vector3 _ y _ ) = y
v3z (Vector3 _ _ z ) = z

concatTuples2 :: [(a, a)] -> [a]
concatTuples2 = foldr folder' [] where
    folder' (a, b)  acc        = a : b : acc
concatTuples3 :: [(a, a, a)] -> [a]
concatTuples3 = foldr folder' [] where
    folder' (a, b, c) acc      = a : b : c : acc
concatTuples4 :: [(a, a, a, a)] -> [a]
concatTuples4 = foldr folder' [] where
    folder' (a, b, c, d) acc   = a : b : c : d : acc
concatVectors3 :: [Vector3 Float] -> [Float]
concatVectors3 = foldr fold' [] where
    fold' (Vector3 x y z) acc = x : y : z : acc
concatVectors4 :: [Vector4 Float] -> [Float]
concatVectors4 = foldr fold' [] where
    fold' (Vector4 x y z w) acc = x : y : z : w : acc

concatVertex1 :: [Vertex1 Float] -> [Float]
concatVertex1 = foldr folder' [] where
    folder' (Vertex1 a) acc = a : acc
concatVertex2 :: [Vertex2 Float] -> [Float]
concatVertex2 = foldr folder' [] where
    folder' (Vertex2 a b) acc = a : b : acc
concatVertex3 :: [Vertex3 Float] -> [Float]
concatVertex3 = foldr folder' [] where
    folder' (Vertex3 a b c) acc = a : b : c : acc
concatVertex4 :: [Vertex4 Float] -> [Float]
concatVertex4 = foldr folder' [] where
    folder' (Vertex4 a b c d) acc = a : b : c : d : acc

-- point-free version of <|>
(a <|.> b) x = a x <|> b x

allPass :: [a -> Bool] -> a -> Bool
allPass fs x = all' fs where
    all'            = check' . find not'
    not' f          = not . f $ x
    check' (Just _) = False
    check' _        = True

