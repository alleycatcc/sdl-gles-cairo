# sdl-gles-cairo

Create graphics apps for desktop & mobile in Haskell, using SDL, OpenGL(ES), & Cairo. Library written in Haskell and a small amount of C.